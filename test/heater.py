import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setwarnings(False)

GPIO.setup(11, GPIO.OUT)


#If Waterfall is on, Switch off
if(GPIO.input(11) == 1):
    GPIO.output(11, False)
    print("Heater is off, switching on...")

#If Waterfall is off, Switch on
elif(GPIO.input(11) == 0):
    GPIO.output(11, True)
    print("Heater is on, switching off...")
