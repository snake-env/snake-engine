import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setwarnings(False)

GPIO.setup(11, GPIO.OUT)

#If Waterfall is on, Switch off
if(GPIO.input(11) == 1):
    print("Heater is off")

#If Waterfall is off, Switch on
elif(GPIO.input(11) == 0):
    print("Heater is on")



