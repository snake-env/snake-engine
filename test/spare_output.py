import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setwarnings(False)

GPIO.setup(13, GPIO.OUT)


#If Waterfall is on, Switch off
if(GPIO.input(13) == 1):
    GPIO.output(13, False)
    print("Spare_output is on, switching off...")

#If Waterfall is off, Switch on
elif(GPIO.input(13) == 0):
    GPIO.output(13, True)
    print("Spare_output is off, switching on...")
