import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setwarnings(False)

GPIO.setup(7, GPIO.OUT)


#If Waterfall is on, Switch off
if(GPIO.input(7) == 1):
    GPIO.output(7, False)
    print("Waterfall is off, switching on...")

#If Waterfall is off, Switch on
elif(GPIO.input(7) == 0):
    GPIO.output(7, True)
    print("Waterfall is on, switching off...")
